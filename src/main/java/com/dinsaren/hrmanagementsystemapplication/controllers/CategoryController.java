package com.dinsaren.hrmanagementsystemapplication.controllers;

import com.dinsaren.hrmanagementsystemapplication.constants.Constant;
import com.dinsaren.hrmanagementsystemapplication.models.Category;
import com.dinsaren.hrmanagementsystemapplication.repositories.CategoryRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CategoryController {
    private final CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("/categories")
    public String index(Model model){
        List<Category> list = categoryRepository.findAllByStatusInOrderByIdDesc(Constant.getAllStatusString());
        model.addAttribute("categories", list);
        System.out.println(list);
        return "category/index";
    }

    @GetMapping("/categories/create")
    public String create(Model model){
        Category category  =  new Category();
        category.setStatusList(Constant.getAllStatus());
        model.addAttribute("category",category);
        return "category/create";
    }

    @PostMapping("/categories/create")
    public String create(Model model, @ModelAttribute("category")Category category){
        if(category.getName() == ""){
            return "category/create";
        }
        categoryRepository.save(category);
        return "redirect:/categories";
    }

    @GetMapping("/categories/edit/{id}")
    public String edit(@PathVariable("id") Integer id, Model model){
        Category category = categoryRepository.findById(id).orElse(null);
        if(category != null) {
            category.setStatusList(Constant.getAllStatus());
            model.addAttribute("category", category);
            return "category/create";
        }
        return "redirect:/categories";
    }

    @GetMapping("/categories/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        Category find = categoryRepository.findById(id).orElse(null);
        if(find != null) {
            find.setStatus("DEL");
            categoryRepository.save(find);
        }
        return "redirect:/categories";
    }
}
