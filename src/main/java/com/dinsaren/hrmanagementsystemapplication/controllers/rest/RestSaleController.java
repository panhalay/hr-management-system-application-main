package com.dinsaren.hrmanagementsystemapplication.controllers.rest;

import com.dinsaren.hrmanagementsystemapplication.models.Sale;
import com.dinsaren.hrmanagementsystemapplication.services.SaleService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import lombok.RequiredArgsConstructor;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class RestSaleController {
    private final Logger log = LoggerFactory.getLogger(RestSaleController.class);
    private final SaleService saleService;
    @GetMapping("/sales")
    public DataTablesOutput<Sale> findAllDatatable(@Valid DataTablesInput input){
        return saleService.findAllDatatable(input);
    }
}
