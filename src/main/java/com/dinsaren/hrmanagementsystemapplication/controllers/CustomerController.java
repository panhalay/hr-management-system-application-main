package com.dinsaren.hrmanagementsystemapplication.controllers;

import com.dinsaren.hrmanagementsystemapplication.constants.Constant;
import com.dinsaren.hrmanagementsystemapplication.models.Customer;

import com.dinsaren.hrmanagementsystemapplication.services.CustomerService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.UUID;
@Slf4j
@Controller
@RequestMapping("/customers")
public class CustomerController {
    @Value("${spring.upload.server.path}")
    private String serverPath;
    @Value("${spring.upload.client.path}")
    private String clintPath;
    private final String customer_path = "/customer";
    private static final String DOT = ".";
    private final CustomerService customerService;
    public CustomerController(CustomerService customerService){
        this.customerService = customerService;
    }
    @GetMapping()
    public String index(Model model){
        model.addAttribute("customers", customerService.getAllCustomerAllStatus());
        return "customer/index";
    }
    @GetMapping("/create")
    public String create(Model model){
        Customer customer = new Customer();
        customer.setStatusList(Constant.getAllStatus());
        model.addAttribute("customer",customer);
        return "customer/create";
    }
    @PostMapping("/create")
    public String create(@ModelAttribute("customer") Customer customer) throws IOException {
        var c = customerService.getCustomerById(customer.getId());
        String writePath = serverPath + this.customer_path;

        File path = new File(writePath);

        if (!path.exists()) {
            path.mkdirs();
        }
        String imageUrl = "";
        MultipartFile file = customer.getFile();
        if (!file.isEmpty()) {
            try {
                String fileName = file.getOriginalFilename();
                fileName = UUID.randomUUID() + "." + (fileName != null ? fileName.substring(fileName.lastIndexOf(".") + 1) : null);
                Files.copy(file.getInputStream(), Paths.get(writePath, fileName));
                imageUrl = "/image" + this.customer_path + "/" + fileName;
                if (c != null && c.getProfile() != null) {
                    Path pathDelete = Paths.get(serverPath + c.getProfile());
                    Files.deleteIfExists(pathDelete);
                }
            } catch (IOException e) {
                log.error("Error upload image :",e);
            }
            if (c != null) {
                customer.setProfile(c.getProfile());
                customer.setUpdatedBy("SYS");
                customer.setUpdatedDate(new Date());
            } else {
                customer.setCreatedBy("SYS");
                customer.setCreatedDate(new Date());
            }
        } else {
            if (c != null) {
                customer.setProfile(c.getProfile());
                customer.setUpdatedBy("SYS");
                customer.setUpdatedDate(new Date());
            } else {
                customer.setCreatedBy("SYS");
                customer.setCreatedDate(new Date());
            }
        }
        if (!"".equals(imageUrl)) {
            customer.setProfile(imageUrl);
        }
        customerService.create(customer);
        return "redirect:/customers";
    }
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model){
        var c =  customerService.getCustomerById(id);
        if(c!=null){
            c.setStatusList(Constant.getAllStatus());
            model.addAttribute("customer", c);
            return "customer/create";
        }

        return "redirect:/customers";
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id){
        customerService.delete(id);
        return "redirect:/customers";
    }
}
