package com.dinsaren.hrmanagementsystemapplication.controllers;

import com.dinsaren.hrmanagementsystemapplication.constants.Constant;
import com.dinsaren.hrmanagementsystemapplication.models.Customer;
import com.dinsaren.hrmanagementsystemapplication.models.Sale;
import com.dinsaren.hrmanagementsystemapplication.repositories.ProductRepo;
import com.dinsaren.hrmanagementsystemapplication.repositories.SaleRepository;
import com.dinsaren.hrmanagementsystemapplication.services.CustomerService;
import com.dinsaren.hrmanagementsystemapplication.services.SaleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Date;


@Controller
public class POSController {
    private final CustomerService customerService;
    private final ProductRepo productRepo;
    private final SaleRepository saleRepository;
    private final SaleService saleService;
    public POSController(CustomerService customerService, ProductRepo productRepo, SaleRepository saleRepository, SaleService saleService){
        this.customerService=customerService;
        this.productRepo = productRepo;
        this.saleRepository = saleRepository;
        this.saleService = saleService;
    }
    @GetMapping("/list-order")
    public String index(){
        return "pos/index";
    }
    @GetMapping("/open-pos")
    public String pos(Model model){
        var customers = customerService.findAllByStatusInOrderByIdAsc(Constant.STATUS_ACTIVE);
        var products = productRepo.findAllByStatusOrderByIdDesc("ACT");
        Sale sale = new Sale();
        sale.setCustomers(customers);
        sale.setProducts(products);
        model.addAttribute("sale",sale);
        return "pos/pos";
    }
    @PostMapping("/open-pos")
    public String pos(@ModelAttribute("sale") Sale sale){
        if (sale.getId()==0){
            sale.setDate(new Date());
        }
        sale.setTotal(saleService.sum(sale));
        saleService.create(sale);
        return "redirect:/list-order";
    }
    @GetMapping("list-order/{id}")
    public String delete(@PathVariable("id") int id){
        var order = saleService.findById(id);
        saleService.delete(order);
        return "redirect:/list-order";
    }

}
