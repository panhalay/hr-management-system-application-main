package com.dinsaren.hrmanagementsystemapplication.models;

import com.dinsaren.hrmanagementsystemapplication.models.request.ItemKeyValue;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

@Entity(name = "pos_customers")
@Getter
@Setter
@Table
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String lastName;
    private String fistName;
    private String email;
    private String phone;
    private String address;
    private String status;
    private String profile;
    @Transient
    private MultipartFile file;
    private Date createdDate;
    private String createdBy;
    private Date updatedDate;
    private String updatedBy;
    @Transient
    List<ItemKeyValue> statusList;
}
