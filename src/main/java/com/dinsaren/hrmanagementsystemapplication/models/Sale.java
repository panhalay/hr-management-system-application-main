package com.dinsaren.hrmanagementsystemapplication.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import java.util.List;

@Entity()
@Table(name = "pos_sales")
@Getter
@Setter
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Date date;
    private double total;
    @ManyToOne
    private Customer customer;
    @Transient
    private List<Customer> customers;
    @ManyToOne
    private Product product;
    @Transient
    private  List<Product> products;
    @Transient
    private List<Double> unit_price;
    @Transient
    private List<Integer> qty;
}
