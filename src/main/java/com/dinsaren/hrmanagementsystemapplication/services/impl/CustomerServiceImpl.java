package com.dinsaren.hrmanagementsystemapplication.services.impl;

import com.dinsaren.hrmanagementsystemapplication.constants.Constant;
import com.dinsaren.hrmanagementsystemapplication.models.Customer;
import com.dinsaren.hrmanagementsystemapplication.repositories.CustomerRepository;
import com.dinsaren.hrmanagementsystemapplication.services.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;
    public CustomerServiceImpl(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }
    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAll();
    }

    @Override
    public List<Customer> getAllCustomerStatusActive() {
        return customerRepository.findAllByStatusOrderByIdDesc(Constant.STATUS_ACTIVE);
    }

    @Override
    public List<Customer> getAllCustomerAllStatus() {
        return customerRepository.findAllByStatusInOrderByIdDesc(Constant.getAllStatusString());
    }

    @Override
    public List<Customer> findAllByStatusInOrderByIdAsc(String status) {
        return customerRepository.findAllByStatusOrderByIdAsc(Constant.STATUS_ACTIVE);
    }
    @Override
    public void create(Customer customer) {
        if(customer.getId()==0){
            customer.setStatus("ACT");
        }
     customerRepository.save(customer);

    }

    @Override
    public void update(Customer customer) {
        var c = customerRepository.findByIdAndStatusIn(customer.getId(),Constant.getAllStatusString());
        if(c !=null){
            customer.setStatus("ACT");
            customerRepository.save(customer);
        }

    }

    @Override
    public Customer getCustomerById(int id) {
        return customerRepository.findByIdAndStatusIn(id,Constant.getAllStatusString());
    }

    @Override
    public void delete(int id) {
        var customer = customerRepository.findByIdAndStatusIn(id,Constant.getAllStatusString());
        if(customer !=null){
           customer.setStatus("DEL");
           customerRepository.save(customer);
        }

    }
}
