package com.dinsaren.hrmanagementsystemapplication.services.impl;

import com.dinsaren.hrmanagementsystemapplication.models.Sale;
import com.dinsaren.hrmanagementsystemapplication.repositories.OrderRepository;
import com.dinsaren.hrmanagementsystemapplication.repositories.ProductRepo;
import com.dinsaren.hrmanagementsystemapplication.repositories.SaleRepository;
import com.dinsaren.hrmanagementsystemapplication.services.SaleService;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

@Service
public class SaleServiceImpl implements SaleService {
    private final SaleRepository saleRepository;
    private final ProductRepo productRepo;
    private final OrderRepository orderRepository;

    public SaleServiceImpl(SaleRepository saleRepository, ProductRepo productRepo,OrderRepository orderRepository) {
        this.saleRepository = saleRepository;
        this.productRepo = productRepo;
        this.orderRepository = orderRepository;
    }

    @Override
    public void create(Sale sale) {
        if (sale.getId() == 0) {
            saleRepository.save(sale);
        }
    }

    @Override
    public void delete(Sale sale) {
        if(sale!=null){
            orderRepository.delete(sale);
        }
    }

    @Override
    public Sale findById(int id) {
        return orderRepository.findById(id);
    }


    @Override
    public double sum(Sale sale) {
        double sum = 0;
        double amount = 0;
        for (double u : sale.getUnit_price()) {
            for (int q : sale.getQty()) {
                if(q !=0){
                    amount= u * q;
                }
            }
            sum+= amount;

        }
        return sum;
    }

    @Override
    public int cutStock(int oldQty, int qtyOut) {
        int currentQty;
        if (oldQty == qtyOut){
            currentQty = 0;
        }else if(oldQty < qtyOut){
            currentQty = qtyOut - oldQty;
        }else{
            currentQty = oldQty - qtyOut;
        }
        return currentQty;
    }

    @Override
    public void setStock(Sale sale) {
//        for (int id: sale.getProduct()) {
//            var p = productRepo.findById(id);
//            for (int q: sale.getQty()) {
//                var current = cutStock(p.getQty(),q);
//                p.setId(id);
//                p.setQty(current);
//            }
//            productRepo.save(p);
//        }
    }

    @Override
    public DataTablesOutput<Sale> findAllDatatable(DataTablesInput input) {
        return saleRepository.findAll(input);
    }

}