package com.dinsaren.hrmanagementsystemapplication.services;
import com.dinsaren.hrmanagementsystemapplication.models.Sale;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface SaleService {
    void create(Sale sale);
    void delete(Sale sale);
    Sale findById(int id);
    public double sum(Sale sale);
   public int cutStock(int oldQty, int qtyOut);
   public void setStock(Sale sale);
  DataTablesOutput<Sale> findAllDatatable(DataTablesInput input);
}
