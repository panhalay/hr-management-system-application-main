package com.dinsaren.hrmanagementsystemapplication.services;
import com.dinsaren.hrmanagementsystemapplication.models.Customer;
import com.dinsaren.hrmanagementsystemapplication.models.Product;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();
    List<Customer> getAllCustomerStatusActive();
    List<Customer> getAllCustomerAllStatus();
    List<Customer> findAllByStatusInOrderByIdAsc(String status);
    void create(Customer customer);
    void update(Customer customer);
    Customer getCustomerById(int id);
    void delete(int id);
}
