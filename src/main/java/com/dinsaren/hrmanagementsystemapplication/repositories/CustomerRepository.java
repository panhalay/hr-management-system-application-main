package com.dinsaren.hrmanagementsystemapplication.repositories;
import com.dinsaren.hrmanagementsystemapplication.models.Customer;
import com.dinsaren.hrmanagementsystemapplication.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    List<Customer> findAllByStatusOrderByIdDesc(String status);
    List<Customer> findAllByStatusOrderByIdAsc(String status);
    List<Customer> findAllByStatusInOrderByIdDesc(List<String> stringList);
    List<Customer> findAllByStatus(String status);

    Customer findByIdAndStatusIn(int id,List<String> stringList);
    Optional<Customer> findById(int id);
}
