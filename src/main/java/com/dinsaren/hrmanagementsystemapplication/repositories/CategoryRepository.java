package com.dinsaren.hrmanagementsystemapplication.repositories;

import com.dinsaren.hrmanagementsystemapplication.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    List<Category> findAllByStatus(String status);
    List<Category> findAllByStatusOrderByIdDesc(String status);
    Category  findByIdAndStatus(Integer id, String status);
    List<Category> findAllByStatusInOrderByIdDesc(List<String> statusList);
}

