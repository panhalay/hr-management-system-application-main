package com.dinsaren.hrmanagementsystemapplication.repositories;

import com.dinsaren.hrmanagementsystemapplication.models.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<Sale,Integer> {
    Sale findById(int id);
}
