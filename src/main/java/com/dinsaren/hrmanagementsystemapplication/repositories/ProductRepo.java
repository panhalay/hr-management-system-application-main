package com.dinsaren.hrmanagementsystemapplication.repositories;
import com.dinsaren.hrmanagementsystemapplication.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepo extends JpaRepository<Product, Integer> {
    List<Product> findAllByStatusInOrderByIdDesc(List<String> statusList);
   List<Product> findAllByStatusOrderByIdDesc(String status);
   List<Product> findAllByStatusOrderByIdAsc(String status);
  Product findById(int Id);
}
